<?php get_header(); ?>

<div id="primary" class="content-area content-single content-single-obra">
	<main id="main" class="site-main">

		<article id="<?php the_ID() ?>" class="post-<?php the_ID() ?>">

			<div class="container">
				<h1>Fotos da obra</h1>
				<?php
				$ano_current = get_field('ano');
				$mes_current = get_field('mes');
				$imovel = get_field('imovel');
				$unique_anos = array();
				$obras_relacionadas = get_posts( array( 
					'post_type' => 'obra',
					'meta_query'	=> array(
						array(
							'key'	 	=> 'imovel',
							'value'	  	=> $imovel->ID,
						)
					),
					'meta_key'			=> 'ano',
					'orderby'			=> 'meta_value_num meta_value',
					'order'				=> 'DESC',
					'posts_per_page' => -1 ) ); ?>

					<div class="date-anos">
						<?php foreach ( $obras_relacionadas as $obra_relacionada ) :
							$ano = get_field('ano',$obra_relacionada->ID);
							if( ! in_array( $ano, $unique_anos ) ) :
								$unique_anos[] = $ano;
								?>
								<a <?php if(get_field('ano',$obra_relacionada->ID) === $ano_current) { echo 'class="current"' ;} ?> href="#" data-ano="<?php the_field('ano',$obra_relacionada->ID); ?>"><?php the_field('ano',$obra_relacionada->ID); ?></a>
							<?php endif;
						endforeach; ?>
					</div>

					<div class="date-meses">
						<?php foreach ( $obras_relacionadas as $obra_relacionada ) :?>
							<a class="<?php the_field('ano',$obra_relacionada->ID); if(get_field('ano',$obra_relacionada->ID) != $ano_current) {echo' hidden';} if(get_field('mes',$obra_relacionada->ID) === $mes_current) {echo' current';} ?>" href="<?php echo get_permalink($obra_relacionada->ID); ?>"><?php echo substr(get_field('mes',$obra_relacionada->ID),0,3); ?></a>
						<?php endforeach; ?>
					</div>

					<?php 
					$images = acf_photo_gallery( 'galeria' , get_the_ID() );
					if ( is_array($images) || is_object($images) ) : ?>
						<div class="owl-container">

							<div class="navigation">
								<div class="navigation-arrows"><div class="navigation-dots"></div></div>
							</div>

							<div class="owl-gallery owl-carousel" id="obrasGallery">

								<?php foreach( $images as $image ): ?>

									<a href="<?php echo $image['full_image_url'] ?>" data-fancybox="galeria" data-caption="<?php echo $image['caption'] ?>" data-dot="<button role='button'><span><?php echo $image['caption'] ?></span></button>">
										<div class="thumb" style="background-image: url(<?php echo acf_photo_gallery_resize_image($image['full_image_url'],720,420) ?>)"></div>
									</a>

								<?php endforeach; ?>

							</div>
						</div>
					<?php endif; ?>
				</div>

			</article>

		</main><!-- #main -->
	</div><!-- #primary -->

	<?php get_template_part( 'template-parts/interesse-single-footer-obra' ) ?>

	<?php get_footer();