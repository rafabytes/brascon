<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordpressGulpBoilerplate
 */

/* Template name: Imóveis */
get_header();
$_SESSION['ids'] = null;
if(!empty($_GET["a"])) { $status = $_GET['status']; }
?>
<div id="primary" class="content-area content-imoveis-list">
	<main id="main" class="site-main">

		<div class="container">
			<h2>IMÓVEIS</h2>
			<div class="open-filters">Filtrar</div>
		</div>

		<div class="container main-container">

			<div class="row">


				<div id="sidebar" class="col">

					<div class="close-filters"></div>

					<div class="sidebar-scroll">

						<div id="loop-options" data-page="1" data-ordem data-intencao data-tipo data-status="<?php echo $status ?>" data-cidade data-bairro data-quartos-min data-quartos-max data-area-min data-area-max></div>

						<?php 
						$quartos_min = array();
						$quartos_max = array();
						$areas_min = array();
						$areas_max = array();
						$imoveis = new WP_Query( array( 
							'post_type' => 'imovel',
							'tax_query'      => array(
								array(
									'taxonomy' => 'status',
									'terms' => array('portfolio'),
									'field' => 'slug',
									'operator' => 'NOT IN'
								)
							),
							'posts_per_page' => -1 ) );
						while ( $imoveis->have_posts() ) :  $imoveis->the_post(); 

							$quarto_min = get_field('quartos_min');
							if(isset($quarto_min) && !empty($quarto_min)){
								$quartos_min[] = $quarto_min; 
							}

							$quarto_max = get_field('quartos_max'); 
							if(isset($quarto_max) && !empty($quarto_max)){
								$quartos_max[] = $quarto_max; 
							}

							$area_min = get_field('area_min'); 
							if(isset($area_min) && !empty($area_min)){
								$areas_min[] = $area_min; 
							}

							$area_max = get_field('area_max'); 
							if(isset($area_max) && !empty($area_max)){
								$areas_max[] = $area_max; 
							}

							wp_reset_postdata(); 

						endwhile; ?>

						<strong>Ordenar por</strong>
						<ul id="filter-ordem">
							<li><a data-ordem="pageviews" href="#">Mais vistos</a></li>
							<li><a data-ordem="preco_de_venda_por" href="#">Por preço</a></li>
							<li><a data-ordem="area" href="#">Por tamanho</a></li>
						</ul>

						<strong>Status do imóvel</strong>
						<ul id="filter-status">
							<li><a class="filter" data-status="lancamento" href="#">Lançamentos</a></li>
							<li><a class="filter" data-status="em-construcao" href="#">Em construção</a></li>
							<li><a class="filter" data-status="pronto-para-morar" href="#">Prontos para morar</a></li>
							<li><a class="filter <?php if($status==="pronto-para-construir") { echo 'current'; } ?>" data-status="pronto-para-construir" href="#">Prontos para construir</a></li>
							<li><a class="filter <?php if($status==="avulso") { echo 'current'; } ?>" data-status="avulso" href="#">Avulsos</a></li>
							<li><a class="filter <?php if($status==="aluguel") { echo 'current'; } ?>" data-status="aluguel" href="#">Aluguéis</a></li>
						</ul>

						<strong>Tipo</strong>
						<ul id="filter-tipo">
							<li><a data-tipo="residencial" href="#">Residencial</a></li>
							<li><a data-tipo="comercial" href="#">Comercial</a></li>
						</ul>

						<strong>Cidade</strong>
						<?php 
						$cidades = get_terms(
							array(
								'taxonomy'   => 'cidade',
								'hide_empty' => true,
								'orderby' => 'name',
								'order' => 'ASC'
							)
						);

						if ( ! empty( $cidades ) && is_array( $cidades ) ) {
							echo '<ul id="filter-cidade" class="multiple">';
							foreach ( $cidades as $cidade ) : ?>
								<li><a href="#" data-cidade="<?php echo $cidade->slug ; ?>"><?php echo $cidade->name; ?></a></li>
								<?php
							endforeach;
							echo '</ul>';
						} 
						?>

						<strong>Bairro</strong>
						<?php 
						$bairros = get_terms(
							array(
								'taxonomy'   => 'bairro',
								'hide_empty' => false,
								'orderby' => 'name',
								'order' => 'ASC'
							)
						);

						if ( ! empty( $bairros ) && is_array( $bairros ) ) {
							echo '<ul id="filter-bairro" class="multiple">';
							foreach ( $bairros as $bairro ) : ?>
								<li><a href="#" data-bairro="<?php echo $bairro->slug ; ?>"><?php echo $bairro->name; ?></a></li>
								<?php
							endforeach;
							echo '</ul>';
						} 
						?>

						<!-- <strong>Status do imóvel</strong>
						<ul id="filter-status" class="multiple">
							<li><a data-status="em-construcao" href="#">Em construção</a></li>
							<li><a data-status="pronto-para-morar" href="#">Pronto para morar</a></li>
							<li><a data-status="lancamento" href="#">Lançamento</a></li>
						</ul> -->

						<div class="slider quartos">
							<strong>Nº de quartos</strong>
							<div id="quartosSlider" class="track" data-min-value="0" data-max-value="<?php echo max($quartos_max) ?>"></div>
							<span id="quartosSliderValue" class="range"></span> quartos
						</div>


						<div class="slider area">
							<strong>Tamanho (m²)</strong>
							<div id="areaSlider" class="track" data-min-value="<?php echo min($areas_min) ?>" data-max-value="<?php echo max($areas_max) ?>"></div>
							<span id="areaSliderValue"></span> m²
						</div>

					</div>

				</div>

				<div id="content" class="col">

					<?php $page_imoveis = get_page_by_path( 'imoveis' ); ?>

					<div class="featured">
						<a href="<?php echo get_field('link',$page_imoveis->ID); ?>">
							<div class="row">
								<div class="col-md-7 image" style="background-image: url(<?php echo get_field('imagem',$page_imoveis->ID) ?>);">

								</div>
								<div class="col-md-5 text">
									<h3><?php echo get_field('titulo',$page_imoveis->ID) ?></h3>
									<p><?php echo get_field('texto',$page_imoveis->ID) ?></p>
									<img src="<?php echo get_template_directory_uri() ?>/images/branding/brascon-icon.png" class="logo-brascon">
								</div>
							</div>
						</a>
					</div>

					<div id="loop-content" class="loading" style="min-height: 50px;"></div>

				</div>

			</div>

		</div>

		<div class="modal fade" id="faleConosco" tabindex="-1" role="dialog" aria-labelledby="faleConoscoLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<?php echo do_shortcode('[contact-form-7 id="333" title="Fale Conosco"]'); ?>
				</div>
			</div>
		</div>

		<?php get_template_part( 'template-parts/interesse' ) ?>

	</main><!-- #main -->
</div><!-- #primary -->
<?php
get_footer();
