<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordpressGulpBoilerplate
 */

/* Template name: Homepage */
get_header();
?>
<div id="primary" class="content-area">
	<main id="main" class="site-main">

		<div class="owl-container">
			
			<div class="backgrounds">
				<div class="bg-lancamentos" style="background-image: url('<?php echo get_field('imagem_box_1') ?>')"></div>
				<div class="bg-construcao" style="background-image: url('<?php echo get_field('imagem_box_2') ?>')"></div>
				<div class="bg-prontos" style="background-image: url('<?php echo get_field('imagem_box_3') ?>')"></div>
			</div>

			<div class="owl-gallery owl-carousel" id="homeSlider" data-autoplay="3000">
			<!-- <div class="owl-gallery owl-carousel" id="homeSlider"> -->

				<?php
				$images = acf_photo_gallery( 'home_slider' , get_the_ID() );
				foreach( $images as $image ): ?>

					<a href="<?php echo $image['url']; ?>"><div class="thumb" style="background-image: url(<?php echo $image['full_image_url'] ?>)" data-dot="<button role='button'><span></span></button>">
						<div class="welcome">
							<div class="content">
								<h1 class="animated fadeInRight"><?php echo $image['title']; ?></h1>
								<h3 class="animated fadeInRight"><?php echo $image['caption']; ?></h3>
							</div>
						</div>
					</div></a>

				<?php endforeach; ?>

			</div>

			<section id="hero" style="background-image: url(<?php echo get_field('imagem_principal') ?>);">

				<div class="container-fluid">

					<div class="tabs">

						<div class="tab collapsed lancamentos animated fadeInRight" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="tabLancamentos">
							<a href="<?php echo get_field('link_box_1') ?>">
								<div class="tab-content" data-bg-target="bg-lancamentos">
									<h3><?php echo get_field('titulo_box_1') ?></h3>
									<div class="collapse" id="tabLancamentos">
										<p><?php echo get_field('texto_box_1') ?></p>
										<a href="<?php echo get_field('link_box_1') ?>" class="button"><?php echo get_field('botao_box_1') ?></a>
									</div>
								</div>
							</a>
						</div>

						<div class="tab collapsed construcao animated fadeInRight" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="tabConstrucao">
							<a href="<?php echo get_field('link_box_2') ?>">
								<div class="tab-content" data-bg-target="bg-construcao">
									<h3><?php echo get_field('titulo_box_2') ?></h3>
									<div class="collapse" id="tabConstrucao">
										<p><?php echo get_field('texto_box_2') ?></p>
										<a href="<?php echo get_field('link_box_2') ?>" class="button"><?php echo get_field('botao_box_2') ?></a>
									</div>
								</div>
							</a>
						</div>

						<div class="tab collapsed prontos animated fadeInRight" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="tabProntos">
							<a href="<?php echo get_field('link_box_3') ?>">
								<div class="tab-content" data-bg-target="bg-prontos">
									<h3><?php echo get_field('titulo_box_3') ?></h3>
									<div class="collapse" id="tabProntos">
										<p><?php echo get_field('texto_box_3') ?></p>
										<a href="<?php echo get_field('link_box_3') ?>" class="button"><?php echo get_field('botao_box_3') ?></a>
									</div>
								</div>
							</a>
						</div>

					</div>

					<div class="navigation">
						<div class="navigation-arrows"><div class="navigation-dots"></div></div>
					</div>

				</div>

			</section>

		</div>
		
	</main><!-- #main -->
</div><!-- #primary -->
<?php
get_footer();
