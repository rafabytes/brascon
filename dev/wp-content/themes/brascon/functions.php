<?php
/**
 * WordpressGulpBoilerplate functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordpressGulpBoilerplate
 */

if ( ! function_exists( 'wpgb_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function wpgb_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on WordpressGulpBoilerplate, use a find and replace
		 * to change 'wpgb' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'wpgb', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'wpgb' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'wpgb_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'wpgb_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function wpgb_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'wpgb_content_width', 640 );
}
add_action( 'after_setup_theme', 'wpgb_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function wpgb_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'wpgb' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'wpgb' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'wpgb_widgets_init' );

add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');
function my_acf_google_map_api( $args ) {
	$args['key'] = 'AIzaSyDbBHqDtxuI3kcrRQYhy7lzpcqn0GOELuM';
	return $args;
}

/**
 * Enqueue scripts and styles.
 */
function wpgb_scripts() {
	wp_enqueue_style( 'wpgb-style', get_stylesheet_uri() );
	wp_enqueue_script( 'gmaps-api', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDbBHqDtxuI3kcrRQYhy7lzpcqn0GOELuM&libraries=places' );
	wp_enqueue_script( 'vendors', get_template_directory_uri() . '/js/vendors.min.js' );
	wp_enqueue_script( 'scripts', get_template_directory_uri() . '/js/scripts.js' );
}
add_action( 'wp_enqueue_scripts', 'wpgb_scripts' );

/**
 * Enqueue Ajax.
 */
function myplugin_ajaxurl() {
	echo '<script type="text/javascript">
	var ajaxurl = "' . admin_url('admin-ajax.php') . '";
	</script>';
}
add_action('wp_head', 'myplugin_ajaxurl');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Register Custom Navigation Walker
 */
function register_navwalker(){
	require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';
}
add_action( 'after_setup_theme', 'register_navwalker' );

/**
 * Add body classes
 */
function add_body_class($classes) {
    // You can modify this check so it will run on every post type
	if (is_page()) {
		global $post;

        // If we *do* have an ancestors list, process it
        // http://codex.wordpress.org/Function_Reference/get_post_ancestors
		if ($parents = get_post_ancestors($post->ID)) {
			foreach ((array)$parents as $parent) {
                // As the array contains IDs only, we need to get each page
				if ($page = get_page($parent)) {
                    // Add the current ancestor to the body class array
					$classes[] = "{$page->post_type}-{$page->post_name}";
				}
			}
		}

        // Add the current page to our body class array
		$classes[] = "{$post->post_type}-{$post->post_name}";
	}

	return $classes;
}
add_filter('body_class', 'add_body_class');

/**
 * Remove Editor
 */
register_nav_menus( array(
	'primary' => __( 'Primary Menu', 'brascon' ),
) );

function remove_editor() {
	if (isset($_GET['post'])) {
		$id = $_GET['post'];
		$template = get_post_meta($id, '_wp_page_template', true);
		switch ($template) {
            // the below removes 'editor' support for 'pages'
            // if you want to remove for posts or custom post types as well
            // add this line for posts:
            // remove_post_type_support('post', 'editor');
            // add this line for custom post types and replace 
            // custom-post-type-name with the name of post type:
            // remove_post_type_support('custom-post-type-name', 'editor');
			case 'template-home.php':
			remove_post_type_support('page', 'editor');
			case 'template-descubra.php':
			remove_post_type_support('page', 'editor');
			case 'template-sobre.php':
			remove_post_type_support('page', 'editor');
			case 'template-contato.php':
			remove_post_type_support('page', 'editor');
			case 'template-imoveis.php':
			remove_post_type_support('page', 'editor');
			break;
			default :
            // Don't remove any other template.
			break;
		}
	}
}
add_action('init', 'remove_editor');

//Exclude pages from WordPress Search
if (!is_admin()) {
	function wpb_search_filter($query) {
		if ($query->is_search) {
			$query->set('post_type', 'post');
		}
		return $query;
	}
	add_filter('pre_get_posts','wpb_search_filter');
}

// Breadcrumbs
function the_breadcrumbs() {

    // Settings
	$separator          = '';
	$breadcrums_id      = 'breadcrumbs';
	$breadcrums_class   = 'breadcrumbs';
	$home_title         = 'Início';
	$blog_url	        = home_url( '/blog' );
	$imoveis_url        = home_url( '/imoveis' );
	$sobre_url	        = home_url( '/sobre' );

    // If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
	$custom_taxonomy    = 'product_cat';

    // Get the query & post information
	global $post,$wp_query;

    // Do not display on the homepage
	if ( !is_front_page() ) {

        // Build the breadcrums
		echo '<ul id="' . $breadcrums_id . '" class="' . $breadcrums_class . '">';

        // Home page
		echo '<li class="item-home"><a class="bread-link bread-home" href="' . get_home_url() . '" title="' . $home_title . '">' . $home_title . '</a></li>';
		echo '<li class="separator separator-home"> ' . $separator . ' </li>';

		if ( is_home() ) {
			echo '<li class="item-current"><strong class="bread-current bread-blog">Conteúdos</strong></li>';
		}

		else if ( is_archive() && !is_tax() && !is_category() && !is_tag() ) {

			echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . post_type_archive_title($prefix, false) . '</strong></li>';

		} else if ( is_archive() && is_tax() && !is_category() && !is_tag() ) {

            // If post is a custom post type
			$post_type = get_post_type();

            // If it is a custom post type display name and link
			if($post_type != 'post') {

				$post_type_object = get_post_type_object($post_type);
				$post_type_archive = get_post_type_archive_link($post_type);

				echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
				echo '<li class="separator"> ' . $separator . ' </li>';

			}

			$custom_tax_name = get_queried_object()->name;
			echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . $custom_tax_name . '</strong></li>';

		} else if ( is_single() ) {

            // If post is a custom post type
			$post_type = get_post_type();

            // If post is a portfolio
			if($post_type == 'portfolio') {

				echo '<li class="item-cat"><a class="bread-cat" href="'.$sobre_url.'">Sobre</a></li>';
				// echo '<li class="separator"> ' . $separator . ' </li>';
				// echo '<li class="item-cat"><a class="bread-cat" href="'.$sobre_url.'#portfolio">Portfólio Brascon</a></li>';
				echo '<li class="separator"> ' . $separator . ' </li>';
				echo '<li class="item-current"><strong class="bread-current">Portfólio Brascon</strong></li>';
				// echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';

			}

			if($post_type == 'obra') {

				$imovel = get_field('imovel');
				$imovel_title = get_the_title($imovel);
				$imovel_url = get_permalink($imovel);
				$ano = get_field('ano');
				$mes = get_field('mes');

				// echo '<li class="item-cat"><a class="bread-cat" href="'.$sobre_url.'">Sobre</a></li>';
				// echo '<li class="item-cat"><a class="bread-cat" href="'.$sobre_url.'#portfolio">Portfólio Brascon</a></li>';
				// echo '<li class="separator"> ' . $separator . ' </li>';
				echo '<li class="item-current"><a class="bread-cat" href="'.$imovel_url.'">'.$imovel_title.'</a></li>';
				echo '<li class="separator"> ' . $separator . ' </li>';
				echo '<li class="item-current">' . $ano . '</li>';
				echo '<li class="separator"> ' . $separator . ' </li>';
				echo '<li class="item-current"><strong class="bread-current">' . $mes . '</strong></li>';
				// echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';

			}

			if($post_type == 'imovel') {

				$imovel = get_field('imovel');
				$imovel_title = get_the_title($imovel);
				$imovel_url = get_permalink($imovel);

				echo '<li class="item-cat"><a class="bread-cat" href="'.$imoveis_url.'">Imóveis</a></li>';
				// echo '<li class="item-cat"><a class="bread-cat" href="'.$sobre_url.'#portfolio">Portfólio Brascon</a></li>';
				echo '<li class="separator"> ' . $separator . ' </li>';
				echo '<li class="item-current"><strong class="bread-current">'.$imovel_title.'</strong></li>';
				// echo '<li class="separator"> ' . $separator . ' </li>';
				// echo '<li class="item-current">' . $ano . '</li>';
				// echo '<li class="separator"> ' . $separator . ' </li>';
				// echo '<li class="item-current"><strong class="bread-current">' . $mes . '</strong></li>';
				// echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';

			}

            // If it is a custom post type display name and link
			if($post_type != 'post') {

				$post_type_object = get_post_type_object($post_type);
				$post_type_archive = get_post_type_archive_link($post_type);

				// echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
				// echo '<li class="separator"> ' . $separator . ' </li>';

			}

            // Get post category info
			$category = get_the_category();

			if(!empty($category)) {

                // Get last category post is in
				$last_category = end(array_values($category));

                // Get parent any categories and create array
				$get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
				$cat_parents = explode(',',$get_cat_parents);

                // Loop through parent categories and store in variable $cat_display
				$cat_display = '';
				foreach($cat_parents as $parents) {
					$cat_display .= '<li class="item-cat item-current"><strong>'.$parents.'</strong></li>';
					// $cat_display .= '<li class="separator"> ' . $separator . ' </li>';
				}

			}

            // If it's a custom post type within a custom taxonomy
			$taxonomy_exists = taxonomy_exists($custom_taxonomy);
			if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {

				$taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
				$cat_id         = $taxonomy_terms[0]->term_id;
				$cat_nicename   = $taxonomy_terms[0]->slug;
				$cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
				$cat_name       = $taxonomy_terms[0]->name;

			}

            // Check if the post is in a category
			if(!empty($last_category)) {
				echo '<li class="item-blog"><a class="bread-link bread-blog" href="'.$blog_url.'">Conteúdos</a></li>';
				echo '<li class="separator"> ' . $separator . ' </li>';
				echo $cat_display;
				// echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';

            // Else if post is in a custom taxonomy
			} else if(!empty($cat_id)) {

				echo '<li class="item-cat item-cat-' . $cat_id . ' item-cat-' . $cat_nicename . ' item-current"><strong class="bread-current bread-cat"><a class="bread-cat bread-cat-' . $cat_id . ' bread-cat-' . $cat_nicename . '" href="' . $cat_link . '" title="' . $cat_name . '">' . $cat_name . '</a></strong></li>';
				// echo '<li class="separator"> ' . $separator . ' </li>';
				// echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';

			} else {

				// echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';

			}

		} else if ( is_category() ) {

            // Category page
			echo '<li class="item-current item-cat"><strong class="bread-current bread-cat">' . single_cat_title('', false) . '</strong></li>';

		} else if ( is_page() ) {

            // Standard page
			if( $post->post_parent ){

                // If child page, get parents 
				$anc = get_post_ancestors( $post->ID );

                // Get parents in the right order
				$anc = array_reverse($anc);

                // Parent page loop
				if ( !isset( $parents ) ) $parents = null;
				foreach ( $anc as $ancestor ) {
					$parents .= '<li class="item-parent item-parent-' . $ancestor . '"><a class="bread-parent bread-parent-' . $ancestor . '" href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
					$parents .= '<li class="separator separator-' . $ancestor . '"> ' . $separator . ' </li>';
				}

                // Display parent pages
				echo $parents;

                // Current page
				echo '<li class="item-current item-' . $post->ID . '"><strong title="' . get_the_title() . '"> ' . get_the_title() . '</strong></li>';

			} else {

                // Just display current page if not parents
				echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '"> ' . get_the_title() . '</strong></li>';

			}

		} else if ( is_tag() ) {

            // Tag page

            // Get tag information
			$term_id        = get_query_var('tag_id');
			$taxonomy       = 'post_tag';
			$args           = 'include=' . $term_id;
			$terms          = get_terms( $taxonomy, $args );
			$get_term_id    = $terms[0]->term_id;
			$get_term_slug  = $terms[0]->slug;
			$get_term_name  = $terms[0]->name;

            // Display the tag name
			echo '<li class="item-current item-tag-' . $get_term_id . ' item-tag-' . $get_term_slug . '"><strong class="bread-current bread-tag-' . $get_term_id . ' bread-tag-' . $get_term_slug . '">' . $get_term_name . '</strong></li>';

		} elseif ( is_day() ) {

            // Day archive

            // Year link
			echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
			echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';

            // Month link
			echo '<li class="item-month item-month-' . get_the_time('m') . '"><a class="bread-month bread-month-' . get_the_time('m') . '" href="' . get_month_link( get_the_time('Y'), get_the_time('m') ) . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</a></li>';
			echo '<li class="separator separator-' . get_the_time('m') . '"> ' . $separator . ' </li>';

            // Day display
			echo '<li class="item-current item-' . get_the_time('j') . '"><strong class="bread-current bread-' . get_the_time('j') . '"> ' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</strong></li>';

		} else if ( is_month() ) {

            // Month Archive

            // Year link
			echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
			echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';

            // Month display
			echo '<li class="item-month item-month-' . get_the_time('m') . '"><strong class="bread-month bread-month-' . get_the_time('m') . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</strong></li>';

		} else if ( is_year() ) {

            // Display year archive
			echo '<li class="item-current item-current-' . get_the_time('Y') . '"><strong class="bread-current bread-current-' . get_the_time('Y') . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</strong></li>';

		} else if ( is_author() ) {

            // Auhor archive

            // Get the author information
			global $author;
			$userdata = get_userdata( $author );

            // Display author name
			echo '<li class="item-current item-current-' . $userdata->user_nicename . '"><strong class="bread-current bread-current-' . $userdata->user_nicename . '" title="' . $userdata->display_name . '">' . 'Author: ' . $userdata->display_name . '</strong></li>';

		} else if ( get_query_var('paged') ) {

            // Paginated archives
			echo '<li class="item-current item-current-' . get_query_var('paged') . '"><strong class="bread-current bread-current-' . get_query_var('paged') . '" title="Page ' . get_query_var('paged') . '">'.__('Page') . ' ' . get_query_var('paged') . '</strong></li>';

		} else if ( is_search() ) {

            // Search results page
			echo '<li class="item-current item-current-' . get_search_query() . '"><strong class="bread-current bread-current-' . get_search_query() . '" title="Buscar">Buscar</strong></li>';

		} elseif ( is_404() ) {

            // 404 page
			echo '<li>' . '404' . '</li>';
		}

		echo '</ul>';

	}

}

/* Filter the except length to 20 words. */
function custom_excerpt_length( $length ) {
	return 20;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

/* Add ... to excerpt */
function change_excerpt_after( $more ) {
	if ( is_admin() ) {
		return $more;
	}

	// Change text, make it link, and return change
	return '...';
}
add_filter( 'excerpt_more', 'change_excerpt_after', 999 );

//page navigation
function numeric_posts_nav( $loop="" , $paged="" ) {

	if( is_singular() )
		return;

	global $wp_query;

	if(empty($loop)) {
		$loop = $wp_query;
	}

	/** Stop execution if there's only 1 page */
	if( $loop->max_num_pages <= 1 )
		return;

	if(empty($paged)) {
		$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
	} else {
		$paged = intval($paged);
	}

	$max   = intval( $loop->max_num_pages );

	/** Add current page to the array */
	if ( $paged >= 1 )
		$links[] = $paged;

	/** Add the pages around the current page to the array */
	if ( $paged >= 3 ) {
		$links[] = $paged - 1;
		$links[] = $paged - 2;
	}

	if ( ( $paged + 2 ) <= $max ) {
		$links[] = $paged + 2;
		$links[] = $paged + 1;
	}

	echo '<ul id="navigation">' . "\n";

	/** Previous Post Link */
	if ( $paged <=1 ) {
		printf( '<li class="disabled">Anterior</li>' );
	} else {
		printf( '<li><a href="#" data-page="'.($paged-1).'">Anterior</a></li>' );
	}

	/** Link to first page, plus ellipses if necessary */
	if ( ! in_array( 1, $links ) ) {
		$class = 1 == $paged ? ' class="number active"' : '';

		printf( '<li class="number"><a href="#" data-page="1">1</a></li>' );

		if ( ! in_array( 2, $links ) )
			echo '<li>…</li>';
	}

	/** Link to current page, plus 2 pages in either direction if necessary */
	sort( $links );
	$i = 1;
	foreach ( (array) $links as $link ) {
		$class = $paged == $link ? ' class="number active"' : '';
		printf( '<li%s class="number"><a href="%s" data-page="'.$link.'">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
		$i++;
	}

	/** Link to last page, plus ellipses if necessary */
	if ( ! in_array( $max, $links ) ) {
		if ( ! in_array( $max - 1, $links ) )
			echo '<li>…</li>' . "\n";

		$class = $paged == $max ? ' class="number active"' : '';
		printf( '<li%s class="number"><a href="%s" data-page="'.$max.'">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
	}

	/** Next Post Link */
	if ( $paged == $max ) {
		printf( '<li class="disabled">Próxima</li>' );
	} else {
		printf( '<li data-page="'.($paged+1).'"><a href="#" data-page="'.($paged+1).'">Próxima</a></li>' );

	}

	echo '</ul>' . "\n";

}
add_action( 'numeric_posts_nav', 'numeric_posts_nav' );

//post filters
add_action( 'wp_ajax_nopriv_load-post-filter', 'prefix_load_cat_posts' );
add_action( 'wp_ajax_load-post-filter', 'prefix_load_cat_posts' );
function prefix_load_cat_posts () {
	// global $wp_query;
	global $post;
	$page = $_POST[ 'page' ];
	$cat_id = $_POST[ 'cat' ];
	$args = array (
		'post_type' => 'post',
		'paged' => $page,
		'cat' => $cat_id,
		'posts_per_page' => 9,
		'order' => 'DESC'

	);

	$loop = new WP_Query( $args );

	ob_start ();

	echo '<div class="d-flex flex-wrap">';
	while ( $loop->have_posts() ) : $loop->the_post();
		
		get_template_part( 'template-parts/article-loop' );

	endwhile;
	echo '</div>';
	numeric_posts_nav($loop,$page);
	wp_reset_query();

	$response = ob_get_contents();
	ob_end_clean();

	echo $response;
	die(1);

}

//imoveis filters
function register_session(){
	if( !session_id() )
		session_start();
}
$_SESSION['ids'] = $ids;
add_action('init','register_session');

add_action( 'wp_ajax_nopriv_load-imoveis-filter', 'prefix_load_cat_imoveis' );
add_action( 'wp_ajax_load-imoveis-filter', 'prefix_load_cat_imoveis' );
function prefix_load_cat_imoveis () {

	// var_dump($_SESSION['ids']);

	// global $wp_query;
	global $post;
	$page = $_POST[ 'page' ];
	$ordem = $_POST[ 'ordem' ];
	$intencao = $_POST[ 'intencao' ];
	$tipo = $_POST[ 'tipo' ];
	$status = $_POST[ 'status' ];
	$cidade = $_POST[ 'cidade' ];
	$bairro = $_POST[ 'bairro' ];
	$quartos_min = $_POST[ 'quartos_min' ];
	$quartos_max = $_POST[ 'quartos_max' ];
	$area_min = $_POST[ 'area_min' ];
	$area_max = $_POST[ 'area_max' ];
	$ordenacao = "DESC";
	$meta_value = "meta_value_num meta_value";

	switch ($ordem) {
		case 'area':
		$ordenacao = "ASC";
		break;
		case 'preco_de_venda_por':
		$ordenacao = "ASC";
		break;
	}

	$tax_query[] = array(
		array(
			'taxonomy' => 'status',
			'terms' => array('lancamento','pronto-para-morar','aluguel','avulso'),
			'field' => 'slug'
		)
	);

	if ( empty($status) && empty($ordem) && empty($tipo) && empty($cidade) && empty($bairro) ) {

		if ($page == 1) {
			$_SESSION['ids'] = "";
		}

		$tax_query[0] = array(
			array(
				'taxonomy' => 'status',
				'terms' => array('lancamento','pronto-para-morar'),
				'field' => 'slug'
			)
		);

		$args = array (
			'post_type' => 'imovel',
			'meta_query'	=> array(
				'relation'		=> 'AND',
				array(
					'relation'		=> 'OR',
					array(
						'key'	 	=> 'quartos_min',
						'value'	  	=> array($quartos_min,$quartos_max),
						'type'    => 'numeric',
						'compare' => 'BETWEEN'
					),
					array(
						'key'	 	=> 'quartos_max',
						'value'	  	=> array($quartos_min,$quartos_max),
						'type'    => 'numeric',
						'compare' => 'BETWEEN'
					)
				),
				array(
					'relation'		=> 'AND',
					array(
						'key'	  	=> 'area_min',
						'value'	  	=> array($area_min,$area_max),
						'type'    => 'numeric',
						'compare' => 'BETWEEN'
					),
					array(
						'key'	  	=> 'area_max',
						'value'	  	=> array($area_min,$area_max),
						'type'    => 'numeric',
						'compare' => 'BETWEEN'
					)
				),
				array(
					'key' => 'ordem',
					'value'   => array(''),
					'compare' => 'NOT IN'
				)
			),
			'tax_query'      => $tax_query,
			'meta_key'			=> 'ordem',
			'orderby'			=> 'meta_value_num meta_value',
			'order'				=> 'ASC',
			'posts_per_page' => -1,
		);

		$loop_brascon = new WP_Query( $args );

		$ids_brascon = wp_list_pluck( $loop_brascon->posts , 'ID' );

		unset($args);

		$args = array (
			'post_type' => 'imovel',
			'meta_query'	=> array(
				'relation'		=> 'AND',
				array(
					'relation'		=> 'OR',
					array(
						'key'	 	=> 'quartos_min',
						'value'	  	=> array($quartos_min,$quartos_max),
						'type'    => 'numeric',
						'compare' => 'BETWEEN'
					),
					array(
						'key'	 	=> 'quartos_max',
						'value'	  	=> array($quartos_min,$quartos_max),
						'type'    => 'numeric',
						'compare' => 'BETWEEN'
					)
				),
				array(
					'relation'		=> 'AND',
					array(
						'key'	  	=> 'area_min',
						'value'	  	=> array($area_min,$area_max),
						'type'    => 'numeric',
						'compare' => 'BETWEEN'
					),
					array(
						'key'	  	=> 'area_max',
						'value'	  	=> array($area_min,$area_max),
						'type'    => 'numeric',
						'compare' => 'BETWEEN'
					)
				)
			),
			'tax_query'      => $tax_query,
			'post__not_in' => $ids_brascon,
			'posts_per_page' => -1,
		);

		$loop_brascon_random = new WP_Query( $args );

		$tax_query[0] = array(
			array(
				'taxonomy' => 'status',
				'terms' => array('aluguel','avulso'),
				'field' => 'slug'
			)
		);

		unset($args);

		$args = array (
			'post_type' => 'imovel',
			'meta_query'	=> array(
				'relation'		=> 'AND',
				array(
					'relation'		=> 'OR',
					array(
						'key'	 	=> 'quartos_min',
						'value'	  	=> array($quartos_min,$quartos_max),
						'type'    => 'numeric',
						'compare' => 'BETWEEN'
					),
					array(
						'key'	 	=> 'quartos_max',
						'value'	  	=> array($quartos_min,$quartos_max),
						'type'    => 'numeric',
						'compare' => 'BETWEEN'
					)
				),
				array(
					'relation'		=> 'AND',
					array(
						'key'	  	=> 'area_min',
						'value'	  	=> array($area_min,$area_max),
						'type'    => 'numeric',
						'compare' => 'BETWEEN'
					),
					array(
						'key'	  	=> 'area_max',
						'value'	  	=> array($area_min,$area_max),
						'type'    => 'numeric',
						'compare' => 'BETWEEN'
					)
				),
				array(
					'key' => 'ordem',
					'value'   => array(''),
					'compare' => 'NOT IN'
				)
			),
			'tax_query'      => $tax_query,
			'meta_key'			=> 'ordem',
			'orderby'			=> 'meta_value_num meta_value',
			'order'				=> 'ASC',
			'posts_per_page' => -1,
		);

		$loop_outros = new WP_Query( $args );

		$ids_outros = wp_list_pluck( $loop_outros->posts , 'ID' );

		unset($args);

		$args = array (
			'post_type' => 'imovel',
			'meta_query'	=> array(
				'relation'		=> 'AND',
				array(
					'relation'		=> 'OR',
					array(
						'key'	 	=> 'quartos_min',
						'value'	  	=> array($quartos_min,$quartos_max),
						'type'    => 'numeric',
						'compare' => 'BETWEEN'
					),
					array(
						'key'	 	=> 'quartos_max',
						'value'	  	=> array($quartos_min,$quartos_max),
						'type'    => 'numeric',
						'compare' => 'BETWEEN'
					)
				),
				array(
					'relation'		=> 'AND',
					array(
						'key'	  	=> 'area_min',
						'value'	  	=> array($area_min,$area_max),
						'type'    => 'numeric',
						'compare' => 'BETWEEN'
					),
					array(
						'key'	  	=> 'area_max',
						'value'	  	=> array($area_min,$area_max),
						'type'    => 'numeric',
						'compare' => 'BETWEEN'
					)
				)
			),
			'tax_query'      => $tax_query,
			'post__not_in' => $ids_outros,
			'posts_per_page' => -1,
		);

		$loop_outros_random = new WP_Query( $args );

		$ids_brascon_random = wp_list_pluck( $loop_brascon_random->posts , 'ID' );
		$ids_outros_random = wp_list_pluck( $loop_outros_random->posts , 'ID' );

		shuffle( $ids_brascon_random );
		shuffle( $ids_outros_random );

		$ids_brascon = array_merge( $ids_brascon , $ids_brascon_random );
		$ids_outros = array_merge( $ids_outros , $ids_outros_random );

		if(!empty($_SESSION['ids'])) {
			$ids = $_SESSION['ids'];
		} else {
			$ids = array_merge( $ids_brascon , $ids_outros );
			$_SESSION['ids'] = $ids;
		}

		// var_dump($ids);

		unset($args);

		$args = array (
			'post_type' => 'imovel',
			'paged' => $page,
			'post__in' => $ids,
			'orderby' => 'post__in',
			'posts_per_page' => 11,
		);
		$loop = new WP_Query( $args );


	} else {

		if (isset($status) && $status !=="") {
			$status = explode(',', $status);
			$tax_query[0] = array(
				'taxonomy' => 'status',
				'terms' => $status,
				'field' => 'slug'
			);
		}

		if (isset($tipo) && $tipo !=="") {
			$tax_query[] = array(
				'taxonomy' => 'tipo',
				'terms' => $tipo,
				'field' => 'slug'
			);
		}

		if (isset($cidade) && $cidade !=="") {
			$cidade = explode(',', $cidade);
			$tax_query[] = array(
				'taxonomy' => 'cidade',
				'terms' => $cidade,
				'field' => 'slug'
			);
		}

		if (isset($bairro) && $bairro !=="") {
			$bairro = explode(',', $bairro);
			$tax_query[] = array(
				'taxonomy' => 'bairro',
				'terms' => $bairro,
				'field' => 'slug'
			);
		}

		$args = array (
			'post_type' => 'imovel',
			'paged' => $page,
			'meta_query'	=> array(
				'relation'		=> 'AND',
				array(
					'relation'		=> 'OR',
					array(
						'key'	 	=> 'quartos_min',
						'value'	  	=> array($quartos_min,$quartos_max),
						'type'    => 'numeric',
						'compare' => 'BETWEEN'
					),
					array(
						'key'	 	=> 'quartos_max',
						'value'	  	=> array($quartos_min,$quartos_max),
						'type'    => 'numeric',
						'compare' => 'BETWEEN'
					)
				),
				array(
					'relation'		=> 'AND',
					array(
						'key'	  	=> 'area_min',
						'value'	  	=> array($area_min,$area_max),
						'type'    => 'numeric',
						'compare' => 'BETWEEN'
					),
					array(
						'key'	  	=> 'area_max',
						'value'	  	=> array($area_min,$area_max),
						'type'    => 'numeric',
						'compare' => 'BETWEEN'
					)
				)
			),
			'tax_query'      => $tax_query,
			'meta_key'			=> $ordem,
			'orderby'			=> $meta_value,
			'order'				=> $ordenacao,
			'posts_per_page' => 8,
		);

		$loop = new WP_Query( $args );

	}

	ob_start ();

	if($loop->have_posts()) {

		echo '<div class="d-flex flex-wrap">';
		while ( $loop->have_posts() ) : $loop->the_post();
			get_template_part( 'template-parts/imovel-loop' );
		endwhile;
		get_template_part( 'template-parts/imovel-loop-after' );
		echo '</div>';
		numeric_posts_nav($loop,$page);

	} else {
		if (isset($status) && $status[0] == "em-construcao") {
			get_template_part( 'template-parts/content-none-imovel-construcao' );
			echo '<div class="d-flex flex-wrap justify-content-center">';
			get_template_part( 'template-parts/imovel-loop-after' );
			echo '</div>';
		} else {
			echo '<div class="d-flex flex-wrap">';
			get_template_part( 'template-parts/imovel-loop-after' );
			echo '</div>';
		// get_template_part( 'template-parts/content-none-imovel' );
		}
	}

	wp_reset_query();

	$response = ob_get_contents();
	ob_end_clean();

	echo $response;
	die(1);

}

function dynamic_field_values ( $tag, $unused ) {

	if ( $tag['name'] != 'imoveis' )
		return $tag;

	$args = array( 
		'post_type' => 'imovel',
		'tax_query'      => array(
			array(
				'taxonomy' => 'status',
				'terms' => array('portfolio'),
				'field' => 'slug',
				'operator' => 'NOT IN',
			)
		),
		'posts_per_page' => -1
	);

	$custom_posts = get_posts($args);

	if ( ! $custom_posts )
		return $tag;

	foreach ( $custom_posts as $custom_post ) {

		$tag['raw_values'][] = $custom_post->post_title;
		$tag['values'][] = $custom_post->post_title;
		$tag['labels'][] = $custom_post->post_title;

	}

	return $tag;

}

add_filter( 'wpcf7_form_tag', 'dynamic_field_values', 10, 2);

add_filter( 'wpcf7_form_elements', 'imp_wpcf7_form_elements' );
function imp_wpcf7_form_elements( $content ) {
	$str_pos = strpos( $content, 'id="imoveis"' );
	if ( $str_pos !== false ) {
		$content = substr_replace( $content, ' data-selected-text-format="count" ', $str_pos, 0 );
	}
	$str_pos = strpos( $content, 'id="uf"' );
	if ( $str_pos !== false ) {
		$content = substr_replace( $content, ' title="UF" ', $str_pos, 0 );
	}
	return $content;
}

//filter by taxonomy on wp-admin
add_action( 'restrict_manage_posts', 'my_restrict_manage_posts' );
function my_restrict_manage_posts() {

    // only display these taxonomy filters on desired custom post_type listings
	global $typenow;
	if ($typenow == 'imovel') {

        // create an array of taxonomy slugs you want to filter by - if you want to retrieve all taxonomies, could use get_taxonomies() to build the list
		$filters = array('status');

		foreach ($filters as $tax_slug) {
            // retrieve the taxonomy object
			$tax_obj = get_taxonomy($tax_slug);
			$tax_name = $tax_obj->labels->name;
            // retrieve array of term objects per taxonomy
			$terms = get_terms($tax_slug);

            // output html for taxonomy dropdown filter
			echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";
			echo "<option value=''>Todos os $tax_name</option>";
			foreach ($terms as $term) {
                // output each select option line, check against the last $_GET to show the current option selected
				echo '<option value='. $term->slug, $_GET[$tax_slug] == $term->slug ? ' selected="selected"' : '','>' . $term->name .' (' . $term->count .')</option>';
			}
			echo "</select>";
		}
	}

	// if ($typenow == 'obra') {

	// 	$filters = array('imovel');

	// 	foreach ($filters as $tax_slug) {
 //            // retrieve the taxonomy object
	// 		$tax_obj = get_taxonomy($tax_slug);
	// 		$tax_name = $tax_obj->labels->name;
 //            // retrieve array of term objects per taxonomy
	// 		$terms = get_terms($tax_slug);

 //            // output html for taxonomy dropdown filter
	// 		echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";
	// 		echo "<option value=''>Todos os $tax_name</option>";
	// 		foreach ($terms as $term) {
 //                // output each select option line, check against the last $_GET to show the current option selected
	// 			echo '<option value='. $term->slug, $_GET[$tax_slug] == $term->slug ? ' selected="selected"' : '','>' . $term->name .' (' . $term->count .')</option>';
	// 		}
	// 		echo "</select>";
	// 	}
	// }

}
