<section id="contact-widget">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="contact-form">
					<h3 class="wow fadeInRight">Tem interesse em um imóvel Brascon?</h3>
					<?php echo do_shortcode('[contact-form-7 id="332" title="Interesse - Todos"]'); ?>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-img wow fadeInUpBig"></div>
			</div>
		</div>
	</div>
	<a href="#" class="qualitare"></a>
</section>