<?php
$tag = get_the_terms(get_the_ID(),'status');
$bairro = get_the_terms(get_the_ID(),'bairro');
$tour = get_field('video_tour_virtual');
?>
<article class="card <?php echo $tag[0]->slug ?> wow fadeInUp" id="post-<?php the_ID(); ?>">
	<a href="<?php  echo get_permalink() ?>">
		<div class="img" style="background-image: url(<?php if( get_field('imagem_do_imovel') ) { echo get_field('imagem_do_imovel'); } else { echo the_post_thumbnail_url('thumbnail'); } ?>);">
			<?php if ( $tag[0]->slug == "lancamento" || $tag[0]->slug == "pronto-para-morar" || $tag[0]->slug == "em-construcao" ) : ?>
				<img src="<?php echo get_template_directory_uri() ?>/images/branding/brascon-icon.png" class="logo-brascon">
			<?php endif; ?>
		</div>
		<?php if ( ! empty( $tag ) ) { echo '<div class="tag">'.$tag->name.'</div>';  } ?>
		<div class="tag"><?php echo $tag[0]->name ?></div>
		<?php if ( ! empty( $tour ) ) { echo '<div class="tag tour">Tour virtual</div>';  } ?>
		<div class="card-body">
			<?php the_title( '<h4 class="card-title">', '</h4>' ); ?>
			<ul class="imovel-details">
				<li class="local"><?php echo $bairro[0]->name ?></li>
				<li class="area"><?php if (get_field('area_min') === get_field('area_max')) { echo get_field('area_min'); } else { echo get_field('area_min').' - '.get_field('area_max'); } ?>m²</li>
				<?php if(get_field('quartos_max') > 0) : ?>
					<li class="quarto"><?php if (get_field('quartos_min') === get_field('quartos_max')) { echo get_field('quartos_min'); } else { echo get_field('quartos_min').' - '.get_field('quartos_max'); } ?> quarto<?php if(get_field('quartos_max') > 1) { echo 's'; } ?> <?php if( get_field('suites') ) { echo ' &nbsp; | &nbsp; ' . get_field('suites') . ' suíte';  } ?><?php if(get_field('suites') > 1) { echo 's'; } ?></li>
				<?php endif ?>
				<?php if (get_field('preco_de_venda_por')) : ?>
					<li class="venda"><strong>VENDA:</strong> <?php the_field('preco_de_venda_por') ?></li>
				<?php endif ?>
				<?php if (get_field('preco_do_aluguel')) : ?>
					<li class="aluguel"><strong>ALUGUEL:</strong> <?php the_field('preco_do_aluguel') ?></li>
				<?php endif ?>
				<?php if (get_field('preco_do_condominio')) : ?>
					<li class="condominio">Condomínio: <?php the_field('preco_do_condominio') ?></li>
				<?php endif ?>
			</ul>
		</div>
	</a>
</article>
