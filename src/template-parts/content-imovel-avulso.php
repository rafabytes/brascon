<div id="primary" class="content-area content-single content-single-imovel content-single-imovel-avulso">
	<main id="main" class="site-main">

		<div class="modal fade" id="interesse" tabindex="-1" role="dialog" aria-labelledby="interesseLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<?php echo do_shortcode('[contact-form-7 id="337" title="Interesse - Single"]'); ?>
				</div>
			</div>
		</div>

		<div class="modal fade" id="tabela" tabindex="-1" role="dialog" aria-labelledby="tabelaLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<?php echo do_shortcode('[contact-form-7 id="348" title="Tabela de preço"]'); ?>
				</div>
			</div>
		</div>

		<article id="<?php the_ID() ?>" class="post-<?php the_ID() ?>">

			<section id="hero" style="background-image: url(<?php echo get_the_post_thumbnail_url()?>)">

				<div class="img-hero d-sm-none" style="background-image: url(<?php echo get_the_post_thumbnail_url()?>)"></div>

				<div class="container">
					<div class="row">
						<div class="col-md-6 content animated fadeInRight">
							<div class="sobre">
								<h3><?php the_title() ?></h3>
								<p><?php the_field('sobre') ?></p>
								<a href="#" class="button black interesse" data-toggle="modal" data-target="#interesse">TENHO INTERESSE</a>
								<a href="#" class="button black precos" data-toggle="modal" data-target="#tabela">TABELA DE PREÇOS</a>
								<?php if(!empty(get_field('tabela_de_precos'))) : ?>
									<a id="tabela" class="d-none" target="_blank" rel="nofollow" href="<?php the_field('tabela_de_precos') ?>">tabela de preços</a>
								<?php endif; ?>
							</div>
						</div>
						<div class="col-md-4 d-none d-sm-block">
							<?php 
							$video = get_field('video');
							if ( $video ) { 
								echo '<a data-fancybox href="'.$video.'" class="video-play white"><span>Veja o vídeo</span></a>';
							}?>
						</div>
					</div>
				</div>

			</section>

			<section id="content">

				<div class="article-body wow fadeInUp">

					<div class="container">

						<div class="row detalhes">
							<div class="col-md-4">
								<h4>Características</h4>
								<ul>
									<?php for ($i=1; $i < 16 ; $i++) { ?>
										<?php if (get_field('titulo_'.$i)): ?>
											<li><span class="icon" style="background-image: url(<?php the_field('icone_'.$i) ?>);"></span><?php the_field('titulo_'.$i) ?></li>
										<?php endif ?>
									<?php } ?>
								</ul>
							</div>
							<?php if( get_field('tipo_2') ) : ?>
								<div class="col-md-4">
									<h4><?php the_field('tipo_2') ?></h4>
									<ul>
										<?php for ($i=1; $i < 16 ; $i++) { ?>
											<?php if (get_field('titulo_2_'.$i)): ?>
												<li><span class="icon" style="background-image: url(<?php the_field('icone_2_'.$i) ?>);"></span><?php the_field('titulo_2_'.$i) ?></li>
											<?php endif ?>
										<?php } ?>
									</ul>
								</div>
							<?php endif ?>
							<?php if( get_field('tipo_3') ) : ?>
								<div class="col-md-4">
									<h4><?php the_field('tipo_3') ?></h4>
									<ul>
										<?php for ($i=1; $i < 16 ; $i++) { ?>
											<?php if (get_field('titulo_3_'.$i)): ?>
												<li><span class="icon" style="background-image: url(<?php the_field('icone_3_'.$i) ?>);"></span><?php the_field('titulo_3_'.$i) ?></li>
											<?php endif ?>
										<?php } ?>
									</ul>
								</div>
							<?php endif ?>
							<?php if( get_field('tipo_4') ) : ?>
								<div class="col-md-4">
									<h4><?php the_field('tipo_4') ?></h4>
									<ul>
										<?php for ($i=1; $i < 16 ; $i++) { ?>
											<?php if (get_field('titulo_4_'.$i)): ?>
												<li><span class="icon" style="background-image: url(<?php the_field('icone_4_'.$i) ?>);"></span><?php the_field('titulo_4_'.$i) ?></li>
											<?php endif ?>
										<?php } ?>
									</ul>
								</div>
							<?php endif ?>
							<?php if( get_field('tipo_5') ) : ?>
								<div class="col-md-4">
									<h4><?php the_field('tipo_5') ?></h4>
									<ul>
										<?php for ($i=1; $i < 16 ; $i++) { ?>
											<?php if (get_field('titulo_5_'.$i)): ?>
												<li><span class="icon" style="background-image: url(<?php the_field('icone_5_'.$i) ?>);"></span><?php the_field('titulo_5_'.$i) ?></li>
											<?php endif ?>
										<?php } ?>
									</ul>
								</div>
							<?php endif ?>
							<?php if (get_field('titulo_6_1')): ?>
								<div class="col-md-4">
									<h4>Outras características</h4>
									<div class="">
										<ul>
											<?php for ($i=1; $i < 16 ; $i++) { ?>
												<?php if (get_field('titulo_6_'.$i)): ?>
													<li><?php the_field('titulo_6_'.$i) ?></li>
												<?php endif ?>
											<?php } ?>
										</ul>
									</div>
								</div>
							<?php endif; ?>
							<?php if( !empty(get_field('preco_de_venda_por')) || !empty(get_field('preco_do_aluguel')) ) : ?>
							<div class="col-md-4">
								<h4>Valores</h4>
								<div class="outras">
									<ul>
										<?php if( !empty(get_field('preco_de_venda_por')) ) : ?>
											<li class="title">Venda</li>
										<?php endif; ?>
										<?php if( !empty(get_field('preco_de_venda_de')) ) : ?>
											<li class="valor_de">De <span><?php the_field('preco_de_venda_de') ?></span> por</li>
										<?php endif; ?>
										<?php if( !empty(get_field('preco_de_venda_por')) ) : ?>
											<li class="valor valorvenda"><?php the_field('preco_de_venda_por') ?></li>
										<?php endif; ?>
										<?php if( !empty(get_field('preco_do_aluguel')) ) : ?>
											<li class="title">Locação</li>
											<li class="valor"><?php the_field('preco_do_aluguel') ?></li>
										<?php endif; ?>
									</ul>
								</div>
							</div>
						<?php endif; ?>
					</div>

				</div>

			</div>

		</section>

		<?php 
		$images = acf_photo_gallery( 'galeria' , get_the_ID() );
		if ( !empty($images) ) : ?>
			<section id="galeria" class="wow fadeIn">

				<div class="owl-container">
					<div class="container">
						<h2>Imagens</h2>
					</div>
					<div class="navigation">
						<div class="navigation-arrows"><div class="navigation-dots"></div></div>
					</div>
					<div class="owl-gallery owl-carousel" id="imovelGallery">

						<?php foreach( $images as $image ): ?>

							<a href="<?php echo $image['full_image_url'] ?>" data-fancybox="galeria" data-caption="<?php echo $image['caption'] ?>" data-dot="<button role='button'><span><?php echo $image['caption'] ?></span></button>">
								<div class="thumb" style="background-image: url(<?php echo acf_photo_gallery_resize_image($image['full_image_url'],720,420) ?>)"></div>
							</a>

						<?php endforeach; ?>

					</div>
				</div>

			</section>
		<?php endif; ?>

		<?php 
		$images = acf_photo_gallery( 'plantas' , get_the_ID() );
		if ( !empty($images) ) : ?>
			<section id="plantas" class="wow fadeIn">

				<div class="container">
					<h2>Plantas</h2>
					<div class="owl-container">
						<div class="owl-gallery owl-carousel" id="plantasGallery">

							<?php foreach( $images as $image ): ?>

								<a href="<?php echo $image['full_image_url'] ?>" data-fancybox="plantas" data-caption="<?php echo $image['caption'] ?>" data-dot="<button role='button'><span><?php echo $image['caption'] ?></span></button>">
									<img class="thumb" src="<?php echo acf_photo_gallery_resize_image($image['full_image_url'],720,420) ?>">
								</a>

							<?php endforeach; ?>

						</div>
						<div class="navigation dark">
							<div class="navigation-arrows"><div class="navigation-dots"></div></div>
						</div>
					</div>
				</div>

			</section>
		<?php endif; ?>

		<?php if( get_field('tour_virtual') ) : ?>

			<section id="tour" style="background-image: url(<?php the_field('imagem_tour') ?>);">

				<div class="container">
					<div class="row">
						<div class="col-sm-6 col-md-5">
							<div class="content wow fadeInUp">
								<h2>Tour virtual</h2>
								<p><?php the_field('tour_virtual') ?></p>
								<a href="#" class="button interesse" data-toggle="modal" data-target="#interesse">TENHO INTERESSE</a>
							</div>
						</div>
						<div class="col-sm-6 col-md-7">
							<a data-fancybox <?php if (strpos(get_field('video_tour_virtual'), 'matterport') !== false) { echo 'data-type="iframe"'; } ?> href="<?php the_field('video_tour_virtual') ?>" class="video-play white"><span>TOUR VIRTUAL</span></a>
						</div>
					</div>
				</div>

			</section>

		<?php endif; ?>

		<section id="mapa" class="wow fadeIn">
			<?php 
			$locais = get_posts( array( 
				'post_type' => 'local',
				'posts_per_page' => -1 ) 
		);
			$endereco = get_field('endereco');
			?>

			<div id="gmaps" data-lat="<?php echo esc_attr($endereco['lat']); ?>" data-lng="<?php echo esc_attr($endereco['lng']); ?>"></div>
			<div class="container">
				<div class="row justify-content-end">
					<div class="col-md-3">
						<div class="content">
							<h4>Você encontra no bairro</h4>
							<ul class="locais">
								<?php foreach ($locais as $local ) : ?>
									<li><a href="#" data-slug="<?php echo $local->slug ?>" class="<?php echo $local->slug ?>"><?php echo get_the_title($local->ID) ?></a></li>
								<?php endforeach ?>
								<li><a href="#" data-slug="all" class="all">Exibir tudo</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</section>


	</article>

</main><!-- #main -->
</div><!-- #primary -->

<?php get_template_part( 'template-parts/interesse-single-footer' ) ?>