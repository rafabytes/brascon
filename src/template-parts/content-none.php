<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordpressGulpBoilerplate
 */

?>

<section class="no-results not-found">
	<div class="page-content">
		<h5>Nenhum resultado encontrado.</h5>
	</div><!-- .page-content -->
</section><!-- .no-results -->
