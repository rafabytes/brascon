<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordpressGulpBoilerplate
 */

?>

<article class="card wow fadeInUp" id="post-<?php the_ID(); ?>">
	<a href="<?php  echo get_permalink() ?>">
		<div class="img"><img class="card-img-top" src="<?php the_post_thumbnail_url('thumbnail') ?>" alt="Card image cap"></div>
		<?php $categories = get_the_category();
		if ( ! empty( $categories ) ) {
			echo '<div class="tag">'.$categories[0]->name.'</div>';   
		} ?>
		<div class="card-body">
			<?php the_title( '<h4 class="card-title">', '</h4>' ); ?>
			<p class="card-meta"><?php wpgb_posted_on(); ?></p>
			<p class="card-text"><?php echo get_the_excerpt() ?></p>
		</div>
	</a>
</article>
